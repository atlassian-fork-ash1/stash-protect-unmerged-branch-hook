<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>4.0.3</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.atlassian.bitbucket.server.plugin</groupId>
    <artifactId>bitbucket-protect-unmerged-branch-hook</artifactId>
    <version>2.0.1-SNAPSHOT</version>

    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/</url>
    </organization>

    <name>Bitbucket Server Protect Unmerged Branch Hook</name>
    <description>A pre-receive hook that stops any push that deletes a branch involved in an active (not merged/declined) pull request.</description>
    <packaging>atlassian-plugin</packaging>

    <properties>
        <bitbucket.version>4.0.0-rc7</bitbucket.version>
        <bitbucket.data.version>${bitbucket.version}</bitbucket.data.version>
        <bitbucket.containerId>tomcat8x</bitbucket.containerId>
        <amps.version>6.1.0</amps.version>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-api</artifactId>
            <version>${bitbucket.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-spi</artifactId>
            <version>${bitbucket.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-git</artifactId>
            <version>${bitbucket.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-util</artifactId>
            <version>${bitbucket.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-model</artifactId>
            <version>${bitbucket.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-test-util</artifactId>
            <version>${bitbucket.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>1.10.19</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>bitbucket-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <products>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket</instanceId>
                            <containerId>${bitbucket.containerId}</containerId>
                            <version>${bitbucket.version}</version>
                            <dataVersion>${bitbucket.data.version}</dataVersion>
                        </product>
                    </products>
                    <instructions>
                        <Bundle-SymbolicName>com.atlassian.bitbucket.server.plugin.hooks.protectbranch</Bundle-SymbolicName>
                        <Import-Package>
                            com.atlassian.bitbucket.*,
                            com.google.common.*
                        </Import-Package>
                        <Private-Package>
                            com.atlassian.bitbucket.server.plugin.hooks.protectbranch
                        </Private-Package>
                    </instructions>
                    <systemPropertyVariables>
                        <!-- Our func tests don't know how to wait for the system to be available, so force
                             it to come up synchronously so that Cargo will block for us -->
                        <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/stash-protect-unmerged-branch-hook.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/stash-protect-unmerged-branch-hook.git</developerConnection>
        <url>https://bitbucket.org/atlassian/stash-protect-unmerged-branch-hook</url>
      <tag>HEAD</tag>
  </scm>

</project>
